import logging
from datasets_rel.json_dataset_rel import JsonDatasetRel

logger = logging.getLogger(__name__)

def train(dataset_name):
    dataset = JsonDatasetRel(dataset_name)
    