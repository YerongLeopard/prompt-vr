conda install pytorch==1.11.0 torchvision==0.12.0 torchaudio cudatoolkit=10.2 -c pytorch
pip install cython
pip install matplotlib numpy scipy pyyaml packaging tensorboardX tqdm pillow scikit-image
conda install opencv
